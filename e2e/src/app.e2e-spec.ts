import { AppPage } from "./app.po";
import { browser, logging, by, protractor } from "protractor";

describe("workspace-project App", () => {
  let page: AppPage;
  var EC = protractor.ExpectedConditions;

  beforeEach(() => {
    page = new AppPage();
  });

  it("should display welcome message", () => {
    page.navigateTo();
    page.connectionButton();
    browser.sleep(4000);
    browser.waitForAngularEnabled(false);
  
    browser.findElement(by.id('login-email')).sendKeys('abdessalem.5e@gmail.com');
    browser.findElement(by.id('login-password')).sendKeys('test1234');
    browser.element(by.cssContainingText(".login-button", "Sign in")).click();
    browser.wait(EC.urlContains('landing'));
    expect(browser.getCurrentUrl()).toContain('/landing');
  });
  
  // afterEach(async () => {
  //   // Assert that there are no errors emitted from the browser
  //   const logs = await browser.manage().logs().get(logging.Type.BROWSER);
  //   expect(logs).not.toContain(jasmine.objectContaining({
  //     level: logging.Level.SEVERE,
  //   } as logging.Entry));
  // });
});
